package ru.semenov.controllers;

import org.springframework.http.ResponseEntity;
import ru.semenov.dto.HoroscopeResponseReference;

import java.util.List;

public interface IHoroscopeRestController<R,P,S,U> extends ICRUDController<R,P,S,U>{
    ResponseEntity<List<P>> getHoroscopeByZodiacSignId(String zodiacSignId);
    ResponseEntity<HoroscopeResponseReference> reference(String zodiacSignName,String horoscopeId);
}
