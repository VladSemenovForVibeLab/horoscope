package ru.semenov.controllers.impl;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.semenov.controllers.IHoroscopeRestController;
import ru.semenov.dto.*;
import ru.semenov.service.ICRUDService;
import ru.semenov.service.IHoroscopeService;

import java.util.List;

@RestController
@RequestMapping(HoroscopeRestController.HOROSCOPE_REST_URI)
public class HoroscopeRestController extends CRUDRestController<HoroscopeRequest, HoroscopeResponse,String, HoroscopeRequestForUpdate> implements IHoroscopeRestController<HoroscopeRequest, HoroscopeResponse,String,HoroscopeRequestForUpdate> {
    public static final String HOROSCOPE_REST_URI = "/api/v1/horoscope";
    private final IHoroscopeService horoscopeService;

    public HoroscopeRestController(IHoroscopeService horoscopeService) {
        this.horoscopeService = horoscopeService;
    }

    @Override
    ICRUDService<HoroscopeRequest, HoroscopeResponse, String,HoroscopeRequestForUpdate> getService() {
        return horoscopeService;
    }

    @Override
    @GetMapping("/byZodiacSignId/{zodiacSignId}")
    public ResponseEntity<List<HoroscopeResponse>> getHoroscopeByZodiacSignId(@PathVariable String zodiacSignId) {
        return new ResponseEntity<>(horoscopeService.findHoroscopeByZodiacSignId(zodiacSignId), HttpStatus.OK);
    }

    @Override
    @PatchMapping("/{horoscopeId}/zodiacSign/{zodiacSignName}")
    public ResponseEntity<HoroscopeResponseReference> reference(@PathVariable String zodiacSignName,
                                                                @PathVariable String horoscopeId) {
        return new ResponseEntity<>(horoscopeService.reference(zodiacSignName,horoscopeId),HttpStatus.OK);
    }
}
