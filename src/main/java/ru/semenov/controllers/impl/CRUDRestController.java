package ru.semenov.controllers.impl;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.semenov.controllers.ICRUDController;
import ru.semenov.dto.MessageResponse;
import ru.semenov.service.ICRUDService;

import java.util.List;

public abstract class CRUDRestController<R,P,S,U> implements ICRUDController<R,P,S,U> {
    abstract ICRUDService<R,P,S,U> getService();
    @Override
    @PostMapping("/create")
    public ResponseEntity<P> createEntity(@RequestBody R requestDTOForCreateEntity){
        return ResponseEntity.ok(getService().createEntity(requestDTOForCreateEntity));
    }
    @Override
    @GetMapping("/findById/{entityId}")
    public ResponseEntity<P> findById(@PathVariable S entityId){
        P entity = getService().findById(entityId);
        if(entity == null){
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(entity);
    }
    @Override
    @GetMapping("/all")
    public ResponseEntity<List<P>> findAll(@RequestParam(defaultValue = "0") int pageNumber,
                                           @RequestParam(defaultValue = "10") int pageSize,
                                           @RequestParam(defaultValue = "") String searchKey){
        List<P> list = getService().findAll(pageNumber, pageSize, searchKey);
        return ResponseEntity.ok(list);
    }
    @Override
    @PatchMapping("/update")
    public ResponseEntity<P> update(U requestDTOForUpdate){
        P update = getService().update(requestDTOForUpdate);
        return ResponseEntity.ok(update);
    }
    @Override
    @DeleteMapping("/delete/{entityId}")
    public ResponseEntity<MessageResponse> delete(@PathVariable S entityId){
        P entityForDelete = getService().findById(entityId);
        return ResponseEntity.ok(getService().delete(entityForDelete));
    }

}
