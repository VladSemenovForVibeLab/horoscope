package ru.semenov.controllers.impl;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.semenov.dto.ZodiacSignRequest;
import ru.semenov.dto.ZodiacSignRequestForUpdate;
import ru.semenov.dto.ZodiacSignResponse;
import ru.semenov.service.ICRUDService;
import ru.semenov.service.IZodiacSignService;

@RestController
@RequestMapping(ZodiacSignRestController.ZODIAC_SIGN_REST_CONTROLLER_URI)
public class ZodiacSignRestController extends CRUDRestController<ZodiacSignRequest, ZodiacSignResponse,String, ZodiacSignRequestForUpdate>{
    public static final String ZODIAC_SIGN_REST_CONTROLLER_URI="/api/v1/zodiacSign";
    private final IZodiacSignService zodiacSignService;

    public ZodiacSignRestController(IZodiacSignService zodiacSignService) {
        this.zodiacSignService = zodiacSignService;
    }

    @Override
    ICRUDService<ZodiacSignRequest, ZodiacSignResponse, String, ZodiacSignRequestForUpdate> getService() {
        return zodiacSignService;
    }
}
