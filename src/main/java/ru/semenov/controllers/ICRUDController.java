package ru.semenov.controllers;

import org.springframework.http.ResponseEntity;
import ru.semenov.dto.MessageResponse;

import java.util.List;

public interface ICRUDController<R,P,S,U> {
    ResponseEntity<P> createEntity(R requestDTOForCreateEntity);
    ResponseEntity<P> findById(S entityId);
    ResponseEntity<List<P>> findAll(int pageNumber,int pageSize,String searchKey);
    ResponseEntity<P> update(U requestDTOForUpdate);
    ResponseEntity<MessageResponse> delete(S entityId);
}
