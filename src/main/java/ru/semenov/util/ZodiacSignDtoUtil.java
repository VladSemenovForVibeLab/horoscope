package ru.semenov.util;

import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.stereotype.Component;
import ru.semenov.dto.ZodiacSignRequest;
import ru.semenov.dto.ZodiacSignRequestForUpdate;
import ru.semenov.dto.ZodiacSignResponse;
import ru.semenov.entity.ZodiacSign;
import ru.semenov.repository.ZodiacSignRepository;
import ru.semenov.service.IHoroscopeService;

import java.util.List;
import java.util.Optional;

@Component
public class ZodiacSignDtoUtil implements EntityDtoUtil<ZodiacSign, ZodiacSignRequest, ZodiacSignResponse, ZodiacSignRequestForUpdate> {
   private final IHoroscopeService horoscopeService;

   private final ZodiacSignRepository zodiacSignRepository;
    public ZodiacSignDtoUtil(IHoroscopeService horoscopeService, ZodiacSignRepository zodiacSignRepository) {
        this.horoscopeService = horoscopeService;
        this.zodiacSignRepository = zodiacSignRepository;
    }

    @Override
    public ZodiacSign toEntityByRequest(ZodiacSignRequest requestDTOForCreateEntity) {
        return ZodiacSign
                .builder()
                .name(requestDTOForCreateEntity.getName())
                .build();
    }

    @Override
    public ZodiacSign toEntityByRequestUpdate(ZodiacSignRequestForUpdate requestDTOForCreateEntity) {
        Optional<ZodiacSign> byId = zodiacSignRepository.findById(requestDTOForCreateEntity.getId());
        if(byId.isPresent()) {
            ZodiacSign zodiacSignIsPresent = byId.get();
            zodiacSignIsPresent.setName(requestDTOForCreateEntity.getName());
            zodiacSignRepository.save(zodiacSignIsPresent);
            return zodiacSignIsPresent;
        }else {
            throw new ResourceNotFoundException("Знак зодиака не найден!");
        }
    }

    @Override
    public ZodiacSignResponse toResponseByEntity(ZodiacSign entitySave) {
        return new ZodiacSignResponse(entitySave);
    }

    @Override
    public List<ZodiacSignResponse> toResponseListByEntityList(List<ZodiacSign> objects) {
        return objects.stream().map(this::toResponseByEntity).toList();
    }

    @Override
    public ZodiacSign toEntityByResponse(ZodiacSignResponse objectDto) {
        return ZodiacSign
                .builder()
                .name(objectDto.getName())
                .build();
    }
}
