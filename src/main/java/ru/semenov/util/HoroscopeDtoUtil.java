package ru.semenov.util;

import org.springframework.stereotype.Component;
import ru.semenov.dto.HoroscopeRequest;
import ru.semenov.dto.HoroscopeRequestForUpdate;
import ru.semenov.dto.HoroscopeResponse;
import ru.semenov.entity.Horoscope;
import ru.semenov.repository.HoroscopeRepository;
import ru.semenov.service.IHoroscopeService;
import ru.semenov.service.IZodiacSignService;

import java.time.LocalDateTime;
import java.util.List;

@Component
public class HoroscopeDtoUtil implements EntityDtoUtil<Horoscope, HoroscopeRequest, HoroscopeResponse, HoroscopeRequestForUpdate> {
    private final IZodiacSignService zodiacSignService;
    private final IHoroscopeService horoscopeService;
    private final HoroscopeRepository horoscopeRepository;

    public HoroscopeDtoUtil(IZodiacSignService zodiacSignService, IHoroscopeService horoscopeService, HoroscopeRepository horoscopeRepository) {
        this.zodiacSignService = zodiacSignService;
        this.horoscopeService = horoscopeService;
        this.horoscopeRepository = horoscopeRepository;
    }

    @Override
    public Horoscope toEntityByRequest(HoroscopeRequest requestDTOForCreateEntity) {
        return Horoscope.builder()
                .prediction(requestDTOForCreateEntity.getPrediction())
                .build();
    }

    @Override
    public Horoscope toEntityByRequestUpdate(HoroscopeRequestForUpdate requestDTOForCreateEntity) {
        Horoscope horoscope = horoscopeRepository.findById(requestDTOForCreateEntity.getId()).get();
        horoscope.setPrediction(requestDTOForCreateEntity.getPrediction());
        return horoscopeRepository.save(horoscope);
    }

    @Override
    public HoroscopeResponse toResponseByEntity(Horoscope entitySave) {
        return new HoroscopeResponse(entitySave);
    }

    @Override
    public List<HoroscopeResponse> toResponseListByEntityList(List<Horoscope> objects) {
        return objects.stream().map(this::toResponseByEntity).toList();
    }

    @Override
    public Horoscope toEntityByResponse(HoroscopeResponse objectDto) {
        return Horoscope
                .builder()
                .prediction(objectDto.getPrediction())
                .build();
    }
}
