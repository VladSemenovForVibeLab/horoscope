package ru.semenov.util;

import java.util.List;

public interface EntityDtoUtil<E, R, P,U> {


    E toEntityByRequest(R requestDTOForCreateEntity);
    E toEntityByRequestUpdate(U requestDTOForCreateEntity);

    P toResponseByEntity(E entitySave);

    List<P> toResponseListByEntityList(List<E> objects);

    E toEntityByResponse(P objectDto);
}
