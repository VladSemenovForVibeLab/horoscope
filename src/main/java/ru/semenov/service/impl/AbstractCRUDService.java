package ru.semenov.service.impl;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.transaction.annotation.Transactional;
import ru.semenov.dto.MessageResponse;
import ru.semenov.service.ICRUDService;
import ru.semenov.util.EntityDtoUtil;

import java.time.LocalDateTime;
import java.util.List;

public abstract class AbstractCRUDService<R, P, S, E,U> implements ICRUDService<R, P, S,U> {
    abstract JpaRepository<E, S> getRepository();

    abstract EntityDtoUtil<E, R, P,U> getEntityDtoUtil();

    @Override
    @Transactional
    public P createEntity(R requestDTOForCreateEntity) {
        E entity = getEntityDtoUtil().toEntityByRequest(requestDTOForCreateEntity);
        E entitySave = getRepository().save(entity);
        return getEntityDtoUtil().toResponseByEntity(entitySave);
    }

    @Override
    @Transactional(readOnly = true)
    public P findById(S entityId) {
        E entityFind = getRepository().findById(entityId).orElseThrow(() -> new ResourceNotFoundException("Данная модель не найдена!"));
        return getEntityDtoUtil().toResponseByEntity(entityFind);
    }

    @Override
    public List<P> findAll(int pageNumber, int pageSize, String searchKey) {
        Pageable pageable = PageRequest.of(pageNumber, pageSize);
        Page<E> entities;
        if (searchKey.equals("")) {
            entities = getRepository().findAll(pageable);
        } else {
            entities = findByPageable(searchKey,pageable);
        }
        List<P> result = getEntityDtoUtil().toResponseListByEntityList(entities.stream().toList());
        return result;
    }

    public abstract Page<E> findByPageable(String searchKey, Pageable pageable);

    @Override
    @Transactional
    public P update(U requestDTOForUpdate) {
        E entityUpdate = getEntityDtoUtil().toEntityByRequestUpdate(requestDTOForUpdate);
        E entitySave = getRepository().save(entityUpdate);
        P dto = getEntityDtoUtil().toResponseByEntity(entitySave);
        return dto;

    }

    @Override
    @Transactional
    public MessageResponse delete(P objectDto) {
        E entityForDelete = getEntityDtoUtil().toEntityByResponse(objectDto);
        getRepository().delete(entityForDelete);
        return MessageResponse.builder()
                .localDateTime(LocalDateTime.now())
                .message("Данная сущность удалена!!")
                .httpStatus(HttpStatus.NO_CONTENT)
                .build();
    }

}

