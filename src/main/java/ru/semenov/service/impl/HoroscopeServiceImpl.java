package ru.semenov.service.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Lazy;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.semenov.dto.*;
import ru.semenov.entity.Horoscope;
import ru.semenov.entity.ZodiacSign;
import ru.semenov.repository.HoroscopeRepository;
import ru.semenov.service.IHoroscopeService;
import ru.semenov.service.IZodiacSignService;
import ru.semenov.util.EntityDtoUtil;
import ru.semenov.util.HoroscopeDtoUtil;
import ru.semenov.util.ZodiacSignDtoUtil;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Set;

@Service
@RequiredArgsConstructor(onConstructor = @__(@Lazy))
public class HoroscopeServiceImpl extends AbstractCRUDService<HoroscopeRequest, HoroscopeResponse,String, Horoscope,HoroscopeRequestForUpdate> implements IHoroscopeService{
    private final HoroscopeRepository horoscopeRepository;
    private final IZodiacSignService zodiacSignService;
    private final HoroscopeDtoUtil horoscopeDtoUtil;
    private final ZodiacSignDtoUtil zodiacSignDtoUtil;

    @Override
    public List<HoroscopeResponse> findHoroscopeByZodiacSignId(String zodiacSignId) {
        ZodiacSignResponse byId = zodiacSignService.findById(zodiacSignId);
        ZodiacSign zodiacSign = zodiacSignDtoUtil.toEntityByResponse(byId);
        return horoscopeDtoUtil.toResponseListByEntityList(horoscopeRepository.findByZodiacSign(zodiacSign));
    }

    @Override
    public Set<Horoscope> findByHoroscopePredications(String horoscopePredications) {
        return horoscopeRepository.findByPredictionContaining(horoscopePredications);
    }

    @Override
    @Transactional
    public HoroscopeResponseReference reference(String zodiacSignName,String horoscopeId) {
        ZodiacSign byName = zodiacSignService.findByName(zodiacSignName);
        Horoscope horoscope = horoscopeRepository.findById(horoscopeId)
                .orElseThrow(()->new ResourceNotFoundException("Не найден!"));
        byName.getHoroscopes().add(horoscope);
        horoscope.setZodiacSign(byName);
        horoscopeRepository.save(horoscope);
        return HoroscopeResponseReference
                .builder()
                .id(horoscopeId)
                .dateCreated(byName.getDateCreated())
                .dateUpdated(LocalDateTime.now())
                .prediction(horoscope.getPrediction())
                .zodiacSignName(byName.getName())
                .build();
    }


    @Override
    JpaRepository<Horoscope, String> getRepository() {
        return horoscopeRepository;
    }

    @Override
    EntityDtoUtil<Horoscope, HoroscopeRequest, HoroscopeResponse, HoroscopeRequestForUpdate> getEntityDtoUtil() {
        return horoscopeDtoUtil;
    }

    @Override
    public Page<Horoscope> findByPageable(String searchKey, Pageable pageable) {
        return horoscopeRepository.findByPredictionContainingIgnoreCase(searchKey,pageable);
    }
}
