package ru.semenov.service.impl;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;
import ru.semenov.dto.ZodiacSignRequest;
import ru.semenov.dto.ZodiacSignRequestForUpdate;
import ru.semenov.dto.ZodiacSignResponse;
import ru.semenov.entity.ZodiacSign;
import ru.semenov.repository.ZodiacSignRepository;
import ru.semenov.service.IZodiacSignService;
import ru.semenov.util.EntityDtoUtil;
import ru.semenov.util.ZodiacSignDtoUtil;

@Service
public class ZodiacSignServiceImpl extends AbstractCRUDService<ZodiacSignRequest, ZodiacSignResponse,String, ZodiacSign, ZodiacSignRequestForUpdate> implements IZodiacSignService {
    private final ZodiacSignRepository zodiacSignRepository;

    private final ZodiacSignDtoUtil zodiacSignDtoUtil;
    public ZodiacSignServiceImpl(ZodiacSignRepository zodiacSignRepository, ZodiacSignDtoUtil zodiacSignDtoUtil) {
        this.zodiacSignRepository = zodiacSignRepository;
        this.zodiacSignDtoUtil = zodiacSignDtoUtil;
    }

    @Override
    JpaRepository<ZodiacSign, String> getRepository() {
        return zodiacSignRepository;
    }

    @Override
    EntityDtoUtil<ZodiacSign, ZodiacSignRequest, ZodiacSignResponse,ZodiacSignRequestForUpdate> getEntityDtoUtil() {
        return zodiacSignDtoUtil;
    }

    @Override
    public Page<ZodiacSign> findByPageable(String searchKey, Pageable pageable) {
        return zodiacSignRepository.findByNameContainingIgnoreCase(searchKey,pageable);
    }

    @Override
    public ZodiacSign findByName(String zodiacSignName) {
        return zodiacSignRepository.findByName(zodiacSignName);
    }
}
