package ru.semenov.service;

import ru.semenov.dto.ZodiacSignRequest;
import ru.semenov.dto.ZodiacSignRequestForUpdate;
import ru.semenov.dto.ZodiacSignResponse;
import ru.semenov.entity.ZodiacSign;

public interface IZodiacSignService extends ICRUDService<ZodiacSignRequest, ZodiacSignResponse,String, ZodiacSignRequestForUpdate> {

    ZodiacSign findByName(String zodiacSignName);
}
