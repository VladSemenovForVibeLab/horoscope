package ru.semenov.service;

import ru.semenov.dto.MessageResponse;

import java.util.List;

public interface ICRUDService<R,P,S,U> {
    P createEntity(R requestDTOForCreateEntity);

    P findById(S entityId);

    List<P> findAll(int pageNumber,int pageSize, String searchKey);

    P update(U requestDTOForUpdate);

    MessageResponse delete(P entityForDelete);

}
