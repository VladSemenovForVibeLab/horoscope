package ru.semenov.service;

import org.springframework.http.HttpStatusCode;
import ru.semenov.dto.*;
import ru.semenov.entity.Horoscope;

import java.util.List;
import java.util.Set;

public interface IHoroscopeService extends ICRUDService<HoroscopeRequest,HoroscopeResponse,String, HoroscopeRequestForUpdate> {

    List<HoroscopeResponse> findHoroscopeByZodiacSignId(String zodiacSignId);

    Set<Horoscope> findByHoroscopePredications(String horoscopePredications);

    HoroscopeResponseReference reference(String zodiacSignName,String horoscopeId);


}
