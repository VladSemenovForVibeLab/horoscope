package ru.semenov.entity;

import jakarta.persistence.*;
import lombok.*;

import java.io.Serializable;
import java.time.LocalDate;

@Entity
@ToString
@EqualsAndHashCode
@Getter
@NoArgsConstructor
@Setter
@Builder
@Table(name = Horoscope.HOROSCOPE_TABLE_NAME)
@AllArgsConstructor
public class Horoscope extends BaseEntity implements Serializable,Comparable<Horoscope> {
    public static final String HOROSCOPE_TABLE_NAME = "horoscopes";
    private static final String HOROSCOPE_COLUMN_NAME_PREDICATION = "predication";
    @Column(name = HOROSCOPE_COLUMN_NAME_PREDICATION, nullable = false)
    private String prediction;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "zodiac_sign_id")
    private ZodiacSign zodiacSign;
    @Override
    public int compareTo(Horoscope o) {
        return this.prediction.compareTo(o.prediction);
    }
}
