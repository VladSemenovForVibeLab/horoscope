package ru.semenov.entity;

import jakarta.persistence.*;
import lombok.*;
import lombok.extern.log4j.Log4j;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.UuidGenerator;

import java.time.LocalDateTime;

@MappedSuperclass
@ToString
@Getter
@Setter
@EqualsAndHashCode
@NoArgsConstructor
public class BaseEntity {
    @Id
    @org.springframework.data.annotation.Id
    @UuidGenerator
    @GenericGenerator(strategy = "uuid2",name = "uuid")
    @GeneratedValue(generator = "system-uuid")
    private String id;
    private LocalDateTime dateCreated;
    private LocalDateTime dateUpdated;
    @PrePersist
    public void onCreate(){
        this.dateCreated = LocalDateTime.now();
        this.dateUpdated= LocalDateTime.now();
    }
    @PreUpdate
    public void onUpdate(){
        this.dateUpdated=LocalDateTime.now();
    }
}
