package ru.semenov.entity;

import jakarta.persistence.*;
import lombok.*;
import org.hibernate.proxy.HibernateProxy;

import java.io.Serializable;
import java.util.Objects;
import java.util.Set;

@Entity
@Getter
@Builder
@Setter
@ToString
@NoArgsConstructor
@NamedEntityGraph(name = "zodiac_entity-graph",attributeNodes = @NamedAttributeNode("horoscopes"))
@Table(name = ZodiacSign.ZODIAC_SIGN_TABLE_NAME)
@AllArgsConstructor
public class ZodiacSign extends BaseEntity implements Comparable<ZodiacSign>,Serializable,Cloneable {
    public static final String ZODIAC_SIGN_TABLE_NAME = "zodiac_sign";
    private static final String ZODIAC_SIGN_COLUMN_NAME = "name";
    @Column(name = ZODIAC_SIGN_COLUMN_NAME,unique = true,nullable = false)
    private String name;
    @Override
    public int compareTo(ZodiacSign o) {
        return this.name.compareTo(o.getName());
    }
    @OneToMany(cascade = CascadeType.ALL,mappedBy = "zodiacSign")
    @ToString.Exclude
    private Set<Horoscope> horoscopes;

    @Override
    public final boolean equals(Object o) {
        if (this == o) return true;
        if (o == null) return false;
        Class<?> oEffectiveClass = o instanceof HibernateProxy ? ((HibernateProxy) o).getHibernateLazyInitializer().getPersistentClass() : o.getClass();
        Class<?> thisEffectiveClass = this instanceof HibernateProxy ? ((HibernateProxy) this).getHibernateLazyInitializer().getPersistentClass() : this.getClass();
        if (thisEffectiveClass != oEffectiveClass) return false;
        ZodiacSign that = (ZodiacSign) o;
        return getId() != null && Objects.equals(getId(), that.getId());
    }

    @Override
    public final int hashCode() {
        return this instanceof HibernateProxy ? ((HibernateProxy) this).getHibernateLazyInitializer().getPersistentClass().hashCode() : getClass().hashCode();
    }
}
