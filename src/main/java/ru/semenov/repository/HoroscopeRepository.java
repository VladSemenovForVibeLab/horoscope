package ru.semenov.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.stereotype.Repository;
import ru.semenov.entity.Horoscope;
import ru.semenov.entity.ZodiacSign;

import java.time.LocalDate;
import java.util.List;
import java.util.Set;

@Repository
@RepositoryRestResource
public interface HoroscopeRepository extends JpaRepository<Horoscope, String> {
    List<Horoscope> findByZodiacSign(ZodiacSign zodiacSign);

    Page<Horoscope> findByPredictionContainingIgnoreCase(String key1 ,Pageable pageable);

    Set<Horoscope> findByPredictionContaining(String horoscopePredications);

}