package ru.semenov.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.ResponseBody;
import ru.semenov.entity.ZodiacSign;

@Repository
@RepositoryRestResource
public interface ZodiacSignRepository extends JpaRepository<ZodiacSign, String> {
    Page<ZodiacSign> findByNameContainingIgnoreCase(String searchKey,Pageable pageable);

    ZodiacSign findByName(String zodiacSignName);
}