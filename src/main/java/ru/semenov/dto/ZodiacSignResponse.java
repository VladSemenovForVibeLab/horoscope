package ru.semenov.dto;

import lombok.*;
import ru.semenov.entity.ZodiacSign;

import java.time.LocalDateTime;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@EqualsAndHashCode
public class ZodiacSignResponse {
    private String id;
    private LocalDateTime dateCreated;
    private LocalDateTime dateUpdated;
    private String name;
    public ZodiacSignResponse(ZodiacSign zodiacSign){
        this.id = zodiacSign.getId();
        this.dateCreated = zodiacSign.getDateCreated();
        this.dateUpdated = zodiacSign.getDateUpdated();
        this.name = zodiacSign.getName();
    }
}
