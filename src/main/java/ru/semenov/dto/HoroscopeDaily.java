package ru.semenov.dto;

import lombok.*;

@ToString
@EqualsAndHashCode
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class HoroscopeDaily {
    private String zodiacSignName;
    private String prediction;
}
