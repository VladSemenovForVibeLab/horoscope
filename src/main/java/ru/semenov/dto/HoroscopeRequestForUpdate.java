package ru.semenov.dto;

import lombok.*;
import org.hibernate.type.internal.ParameterizedTypeImpl;
import ru.semenov.entity.Horoscope;

import java.io.Serializable;

@Getter
@Setter
@ToString
@AllArgsConstructor
@EqualsAndHashCode
@Builder
public class HoroscopeRequestForUpdate implements Serializable,Comparable<HoroscopeRequestForUpdate> {
    private String prediction;
    private String id;
    private HoroscopeRequestForUpdate(){}

    @Override
    public int compareTo(HoroscopeRequestForUpdate o) {
        return this.prediction.compareTo(o.prediction);
    }
    public HoroscopeRequestForUpdate(Horoscope horoscope){
        this.prediction = horoscope.getPrediction();
        this.id = horoscope.getId();
    }
}
