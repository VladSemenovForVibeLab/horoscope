package ru.semenov.dto;

import lombok.*;
import ru.semenov.entity.ZodiacSign;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@EqualsAndHashCode
public class ZodiacSignRequestForUpdate {
    private String name;
    private String id;
    public ZodiacSignRequestForUpdate(ZodiacSign zodiacSign){
        this.name = zodiacSign.getName();
    }
}
