package ru.semenov.dto;

import lombok.*;

import java.time.LocalDateTime;
@Getter
@Setter
@ToString
@AllArgsConstructor
@EqualsAndHashCode
@Builder
public class HoroscopeResponseReference {
    private String id;
    private LocalDateTime dateCreated;
    private LocalDateTime dateUpdated;
    private String prediction;
    private String zodiacSignName;
}
