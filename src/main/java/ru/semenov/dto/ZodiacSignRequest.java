package ru.semenov.dto;

import lombok.*;
import ru.semenov.entity.Horoscope;
import ru.semenov.entity.ZodiacSign;

import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
@EqualsAndHashCode
public class ZodiacSignRequest {
    private String name;
    public ZodiacSignRequest(ZodiacSign zodiacSign){
        this.name = zodiacSign.getName();
    }
}
