package ru.semenov.dto;

import lombok.*;
import ru.semenov.entity.Horoscope;

import java.io.Serializable;
import java.time.LocalDateTime;

@Getter
@Setter
@AllArgsConstructor
@ToString
@EqualsAndHashCode
public class HoroscopeResponse implements Serializable,Comparable<HoroscopeResponse> {
    private String id;
    private LocalDateTime dateCreated;
    private LocalDateTime dateUpdated;
    private String prediction;

    @Override
    public int compareTo(HoroscopeResponse o) {
        return this.prediction.compareTo(o.prediction);
    }
    public HoroscopeResponse(Horoscope horoscope){
        this.id = horoscope.getId();
        this.dateCreated = horoscope.getDateCreated();
        this.dateUpdated    = horoscope.getDateUpdated();
        this.prediction = horoscope.getPrediction();
    }
}
