package ru.semenov.dto;

import jakarta.annotation.security.DenyAll;
import lombok.*;
import org.apache.commons.lang3.builder.ToStringExclude;
import org.springframework.http.HttpStatus;
import ru.semenov.entity.ZodiacSign;

import java.time.LocalDateTime;

@Getter
@Setter
@NoArgsConstructor
@Builder
@AllArgsConstructor
@ToString
@EqualsAndHashCode
public class MessageResponse {
    private String message;
    private HttpStatus httpStatus;
    private LocalDateTime localDateTime;
}
