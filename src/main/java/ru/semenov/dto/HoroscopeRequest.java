package ru.semenov.dto;

import lombok.*;
import ru.semenov.entity.Horoscope;

import java.io.Serializable;

@Getter
@Setter
@ToString
@AllArgsConstructor
@EqualsAndHashCode
@Builder
public class HoroscopeRequest implements Serializable,Comparable<HoroscopeRequest> {
    private String prediction;
    private HoroscopeRequest(){}

    @Override
    public int compareTo(HoroscopeRequest o) {
        return this.prediction.compareTo(o.prediction);
    }
    public HoroscopeRequest(Horoscope horoscope){
        this.prediction = horoscope.getPrediction();
    }
}
